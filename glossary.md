# Glossary

## Unit testing

> The type of testing where a developer (usually the one who wrote the
> code) proves that a code module (the "unit") meets its requirements.

-- [Free On-Line Dictionary of Computing](https://foldoc.org/unit%20testing)



> The primary goal of unit testing is to take the smallest piece of testable
> software in the application, isolate it from the remainder of the code,
> and determine whether it behaves exactly as you expect. ...
> Unit testing has proven its value in that a large percentage of defects
> are identified during its use

-- [Microsoft Developer Network](https://msdn.microsoft.com/en-us/library/aa292197(v=vs.71).aspx)


> In computer programming, unit testing is a software testing method
> by which individual units of source code ...  are tested to determine
> whether they are fit for use.

-- [Wikipedia](https://en.wikipedia.org/wiki/Unit_testing)

See also: https://en.wikipedia.org/wiki/XUnit

## Integration Testing

> A type of testing in which software and/or hardware components are
> combined and tested to confirm that they interact according to their
> requirements. Integration testing can continue progressively until the
> entire system has been integrated.

-- [Free On-Line Dictionary of Computing](https://foldoc.org/integration%20testing)

> Integration testing ... is the phase in software testing in
> which individual software modules are combined and tested as a
> group. ... Integration testing takes as its input modules that have been
> unit tested, groups them in larger aggregates, applies tests defined in
> an integration test plan to those aggregates, and delivers as its output
> the integrated system ready for system testing.

-- [Wikipedia](https://en.wikipedia.org/wiki/Integration_testing)

## CI

See [Continuous Integration](#continuous-integration)

## Continuous Integration


> Continuous integration (CI) is the practice of merging all developer
> working copies to a shared mainline several times a day.
...
> The main aim of CI is to prevent integration problems

-- [Wikipedia](https://en.wikipedia.org/wiki/Continuous_integration)

References:

- Article [Continuous Integration](https://martinfowler.com/articles/continuousIntegration.html) by Martin Fowler, Chief Scientist at ThoughtWorks, which created CruiseControl, the first Continuous Integration server (2001).

## CD

CD stands for:
- [Continuous Deployment](#continuous-depoyment)
- [Continuous Delivery](#continuous-delivery)

## Continuous Deployment

> Continuous Deployment is closely related to Continuous Integration
> and refers to the release into production of software that passes the
> automated tests.

-- [ThoughtWorks.com](https://www.thoughtworks.com/continuous-integration)

## Continuous Delivery

> Continuous delivery is sometimes confused with continuous deployment. Continuous deployment means that every change is automatically deployed to production. Continuous delivery means that the team ensures every change can be deployed to production but may choose not to do it, usually due to business reasons. In order to do continuous deployment one must be doing continuous delivery.
-- [Wikipedia](https://en.wikipedia.org/wiki/Continuous_delivery)

See also:

- https://www.quora.com/What-is-the-difference-between-continuous-delivery-and-continuous-deployment

## Pipeline

> A linear sequence of specialized modules used for pipelining

-- [Oxford Dictionaries](https://en.oxforddictionaries.com/definition/us/pipeline)

> Pipelining: A form of computer organization in which successive steps of an instruction sequence are executed in turn by a sequence of modules able to operate concurrently, so that another instruction can be begun before the previous one is finished.

-- [Oxford Dictionaries](https://en.oxforddictionaries.com/definition/us/pipelining)


> A system through which something is conducted, especially as a means of supply: "Farther down the pipeline are three other approaches to vaccine development" (Boston Globe).

-- [American Heritage Dictionary of the English Language](https://www.ahdictionary.com/word/search.html?q=pipeline)

https://martinfowler.com/bliki/DeploymentPipeline.html
