Pause your Docker runner and register another Shell runner. We'll need it to deploy code to our stage and prod environments. 

# [[Next]](91-set-up-prod-and-stg-web-sites.md) [[Up]](README.md)
