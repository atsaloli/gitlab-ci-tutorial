# Disable GitLab.com Shared Runners

To better understand how GitLab CI works and how it fits in with GitLab,
(and also so we don't have to wait for the public runners on GitLab.com
courtesy of DigitalOcean), let's disable the GitLab.com shared runners 
and use our own.

Go to `Settings -> Pipelines -> Shared Runners -> Disable shared runners`.

# [[Up]](README.md)
