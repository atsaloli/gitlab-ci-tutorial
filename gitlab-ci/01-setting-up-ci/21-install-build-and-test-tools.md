# Install build and test tools

To mock up build and test jobs, we'll need build and test tools.

On the Runner Server host, run:

```
sudo apt install -y php7.0-cli php7.0-fpm phpunit build-essential
```

# [[Next]](21b-runners-chapter-title.md) [[Up]](README.md)
