# Setting up your CI/CD infrastructure

- [Architecture: GitLab Server + Runner Server](05-architecture.md)
- [Install GitLab Server](10-installing-gitlab-server.md)
- [Add a project](12-setting-up-a-project.md)
- [Set up CI](13-enabling-ci-on-a-project.md)
- [Install Docker](15-installing-docker.md)
- [Install Runner Server](20-installing-gitlab-ci.md)
- [Install build/test tools](21-install-build-and-test-tools.md)

## Registering, enabling and using runners; reading job logs
- [Register a Shell runner](22-registering-our-first-runner.md)
- [Unregister the Shell runner](24-unregistering-runners.md)
- [Register a Docker runner](25-register-and-enable-Docker-runner.md)
- [Test Docker runner](26-test-docker-runner.md)
- [Use a different container image](27-change-docker-image.md)
- [Runner administration](80-runners-admin.md)
- [Paused runner](84-paused-runner.md)
- [Pause Docker runner and register Shell runner](86-shell-again.md)

## Set up deployment targets: Stage and Prod
- [Set up Stage and Prod web sites](91-set-up-prod-and-stg-web-sites.md)
- [Set up trust relationship to deploy to Stage via SSH](92-deploy-using-ssh.md)
- [Set up trust relationships to deploy to Stage and Prod via Git](93-deploy-via-git.md)


# [[Up]](../README.md)
