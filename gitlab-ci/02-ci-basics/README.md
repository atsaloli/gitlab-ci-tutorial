# GitLab CI/CD Basics
- [Definitions of Key Terms](00-key-definitions.md)
- Continuous Integration
  - [CI config: setting up a test job](01-single-test-job.md)
  - [CI config: setting up a pipeline (Shell)](02-simple-3-stage-shell-pipeline.md)
  - [CI config: setting up a pipeline (Docker)](03-simple-3-stage-docker-pipeline.md)
  - [CI config: setting up a pipeline with multiple jobs per stage](05-ci.md)
  - [Concurrency: build admin page](10-builds-admin.md)
  - [Concurrency: enabling job parallelization](20-concurrent-builds.md)
  - [CI config: customizing your pipeline stages](30-custom-stages.md)
  - [CI config: integrating with an external test framework (PHPUnit)](40-testing-with-phpunit.md)
- Continuous Delivery
  - [Continuous Delivery: deploying to staging environment (via SSH)](42-deploying-to-stage-via-ssh.md)
  - [Continuous Delivery: set up git pulls from mock environments](44-set-up-git-pulls-from-mock-envs.md)
  - [Continuous Delivery: deploying to staging environment (via Git)](45-deploying-to-stage-via-git.md)
  - [Continuous Delivery: manual deploy to production](50-manual-deploy-to-production.md)
  - [Continuous Deployment: automatic deploy to production](55-continuous-deployment.md)
- Troubleshooting
  - [Enabling verbose builds](90-debugging-builds.md)
  - [Increasing Runner Server "log_level"](100-increasing-loglevel.md)
  - [Checking GitLab Server logs](110-gitlab-logs.md)
  - [Interactive access to containers](115-interactive-containers.md)
  - [Checking environment variables](118-env-vars.md)
  - [Debugging Tips](120-debugging.md)


# [[Next]](00-key-definitions.md) [[Up]](../README.md)
