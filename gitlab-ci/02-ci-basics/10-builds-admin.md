# Jobs Admin is at /admin/jobs

Notice that the "test" stage doesn't start until the "build" stage completes.

Also notice only one job is running at a time.

![Jobs Admin page](../images/builds-admin.png)

# [[Next]](20-concurrent-builds.md) [[Up]](README.md)
