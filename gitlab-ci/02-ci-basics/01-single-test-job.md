# Simplest CI YAML

```yaml
i_love_tests:

  script: /bin/echo run tests
```

![single test job](../images/single-test-job.png)

# [[Next]](02-simple-3-stage-shell-pipeline.md) [[Up]](README.md)
