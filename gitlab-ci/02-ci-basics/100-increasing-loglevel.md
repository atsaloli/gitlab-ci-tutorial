# Increasing Runner Server log_level

You can increase Runner Server `log_level` in `/etc/gitlab-runner/config.toml`.

See https://gitlab.com/gitlab-org/gitlab-runner/blob/master/docs/configuration/advanced-configuration.md

# [[Next]](110-gitlab-logs.md) [[Up]](README.md)
