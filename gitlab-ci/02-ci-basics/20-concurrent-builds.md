# Build Concurrency

The default build concurrency is 1.

Increase it in /etc/gitlab-runner/config.toml

Do you need to reload the `gitlab-ci-multi-runner` service?

![Concurrent builds](../images/concurrent-builds.png)

Do your multiple test jobs run in parallel now?

# [[Next]](30-custom-stages.md) [[Up]](README.md)
