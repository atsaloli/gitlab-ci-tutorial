# Setting up CI/CD Pipelines


Aleksey Tsalolikhin

aleksey@verticalsysadmin.com

28 October 2017

## Table of Contents
- [Part I - Introduction to CI/CD and DevOps](https://gitpitch.com/atsaloli/cicd?grs=gitlab)
- [Part II - GitLab CI/CD Tutorial](https://gitlab.com/atsaloli/gitlab-ci-tutorial/blob/master/gitlab-ci/README.md)
- [Part III - Jenkins Tutorial](https://gitlab.com/atsaloli/gitlab-ci-tutorial/blob/master/jenkins/README.md)
